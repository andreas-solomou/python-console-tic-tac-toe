# Created with Python v.3.8.2 by Andreas Solomou March 2020
# basic Tic Tac Toe with clearScreen functionalitys

# Default Data
board = [' ' for x in range(10)]

#=======================================

# Display Controls
def exampleBoard():
    print('\n\n\n')
    print('                        7|8|9')
    print('                        -----')
    print('                        4|5|6')
    print('                        -----')
    print('                        1|2|3')
    print('\n')

def printBoard(board):
    clearScreen()
    print('\n\n\n')
    print('                        ' + board[7] + '|' + board[8] + '|' + board[9])
    print('                        -----')
    print('                        ' + board[4] + '|' + board[5] + '|' + board[6])
    print('                        -----')
    print('                        ' + board[1] + '|' + board[2] + '|' + board[3])
    print('\n')

def firstScreen():
    clearScreen()
    print('\n')
    print('      OOOOOOOO     OO       OOOO  ')
    print('         OO        OO     OO      ')
    print('         OO        OO     OO      ')
    print('         OO        OO     OO      ')
    print('         OO        OO       OOOO  ')
    print('\n')
    print('      OOOOOOOO    OOOO      OOOO')
    print('         OO     OO    OO  OO')
    print('         OO     OO OO OO  OO    ')
    print('         OO     OO    OO  OO    ')
    print('         OO     OO    OO    OOOO')
    print('\n')
    print('      OOOOOOOO    OOOO    OOOOOO')
    print('         OO     OO    OO  OO')
    print('         OO     OO    OO  OOOOO')
    print('         OO     OO    OO  OO')
    print('         OO       OOOO    OOOOOO')
    print('\n\n')
    try:
        raw_input('      >> press ENTER to START <<\n\n      ')
    except:
        input('      >> press ENTER to START <<\n\n      ')

def hintScreen():
    clearScreen()
    exampleBoard()
    print('      Hint: Type a number between 1-9 to fill the\n      corresponding field\n')
    try:
        raw_input('      >> press ENTER when ready to CONTINUE <<\n\n      ')
    except:
        input('      >> press ENTER when ready to CONTINUE <<\n\n      ')

def clearScreen():
    import os
    import platform
    if (platform.system() == 'Windows'):
        clear = lambda: os.system('cls')
    else:
        clear = lambda: os.system('clear')
    clear()

#=======================================

# Misc Game Functionality
def addPlayerMove(letter, pos):
    board[pos] = letter

def posIsFree(pos):
    return board[pos] == ' '

def isWinner(bo, le):
    return (bo[7] == le and bo[8] == le and bo[9] == le) or (bo[4] == le and bo[5] == le and bo[6] == le) or(bo[1] == le and bo[2] == le and bo[3] == le) or(bo[1] == le and bo[4] == le and bo[7] == le) or(bo[2] == le and bo[5] == le and bo[8] == le) or(bo[3] == le and bo[6] == le and bo[9] == le) or(bo[1] == le and bo[5] == le and bo[9] == le) or(bo[3] == le and bo[5] == le and bo[7] == le)

def selectRandom(li):
    import random
    ln = len(li)
    r = random.randrange(0,ln)
    return li[r]

def isBoardFull(board):
    if board.count(' ') > 1:
        return False
    else:
        return True

#=======================================

# Handles Player Move
def playerMove():
    run = True
    while run:
        move = input('      Please select a position to place an \'X\' (1-9): ')
        try:
            move = int(move)
            if move > 0 and move < 10:
                if posIsFree(move):
                    run = False
                    addPlayerMove('X', move)
                else:
                    print('      Sorry, this space is occupied!')
            else:
                print('      Please type a number within the range!')
        except:
            print('      Please type a number!')

# Game AI
def compMove():
    possibleMoves = [x for x, letter in enumerate(board) if letter == ' ' and x != 0]
    move = 0

    for let in ['O', 'X']:
        for i in possibleMoves:
            boardCopy = board[:]
            boardCopy[i] = let
            if isWinner(boardCopy, let):
                move = i
                return move

    cornersOpen = []
    for i in possibleMoves:
        if i in [1,3,7,9]:
            cornersOpen.append(i)

    if len(cornersOpen) > 0:
        move = selectRandom(cornersOpen)
        return move

    if 5 in possibleMoves:
        move = 5
        return move

    edgesOpen = []
    for i in possibleMoves:
        if i in [2,4,6,8]:
            edgesOpen.append(i)

    if len(edgesOpen) > 0:
        move = selectRandom(edgesOpen)

    return move

#=======================================

# Main Function
def main():
    print('   Welcome to Tic Tac Toe!')
    printBoard(board)

    while not(isBoardFull(board)):
        if not(isWinner(board, 'O')):
            playerMove()
            printBoard(board)
        else:
            print('      Sorry, O\'s won this time!')
            break

        if not(isWinner(board, 'X')):
            move = compMove()
            if move == 0:
                print('      Tie Game!')
            else:
                addPlayerMove('O', move)
                print('      Computer placed an \'O\' in position', move , ':')
                printBoard(board)
        else:
            print('      X\'s won this time! Good Job!')
            break

#=======================================

# Execution
firstScreen()
hintScreen()
main()
while True:
    try:
        answer = raw_input('\n      START NEW GAME? (Y/N): ')
    except:
        answer = input('\n      START NEW GAME? (Y/N): ')
    if answer.lower() == 'y' or answer.lower == 'yes':
        board = [' ' for x in range(10)]
        print('-----------------------------------')
        main()
    elif answer.lower() == 'n' or answer.lower == 'no':
        clearScreen()
        print('Thank you for playing :)')
        break
    else:
        try:
            answer = raw_input('\n      START NEW GAME? (Y/N): ')
        except:
            answer = input('\n      START NEW GAME? (Y/N): ')

#=======================================
